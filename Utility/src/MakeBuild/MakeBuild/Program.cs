﻿using System;
using System.IO;
using System.Reflection;

namespace MakeBuild
{
    class Program
    {
        static void Main(string[] args)
        {
            string ItemGenPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\..\\ItemGen\\";
            string UpdaterPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\..\\Updater\\";

            string buildPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\..\\ItemGen\\bin\\Release\\build\\";

            if (args.Length >= 1)
                buildPath = args[0] + "\\";

            if (!Directory.Exists(ItemGenPath + "Resources"))
            {
                Console.WriteLine("App is in wrong path!"); ;
            }
            else
            {
                if (!Directory.Exists(buildPath))
                    Directory.CreateDirectory(buildPath);

                if (!Directory.Exists(buildPath + "lang"))
                    Directory.CreateDirectory(buildPath + "lang");

                foreach (string newPath in Directory.GetFiles(ItemGenPath + "Resources\\lang", "*.*", SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(ItemGenPath + "Resources\\lang", buildPath + "lang"), true);

                if (File.Exists(buildPath + "ItemGen.exe"))
                    File.Delete(buildPath + "ItemGen.exe");

                File.Copy(ItemGenPath + "\\Bin\\Release\\ItemGen.exe", buildPath + "ItemGen.exe");

                if (File.Exists(buildPath + "Updater.exe"))
                    File.Delete(buildPath + "Updater.exe");

                File.Copy(UpdaterPath + "\\Bin\\Release\\Updater.exe", buildPath + "Updater.exe");
            }
        }
    }
}
