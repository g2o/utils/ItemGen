﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Net;

namespace Updater
{
    class Updater
    {
        private static WebClient webClient;

        private static string appName;
        private static string repositoryURL;
        private static string appPath;
        private static string errorMsg;

        private static void UpdateExe()
        {
            string tmpPath = Path.GetTempFileName();

            try
            {
                webClient.DownloadFile(repositoryURL + "ItemGen/bin/Release/" + appName + ".exe", tmpPath);
            }
            catch (WebException)
            {
                MessageBox.Show(errorMsg, appName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                Thread.Sleep(1000);

                if (File.Exists(appPath))
                    File.Delete(appPath);

                File.Move(tmpPath, appPath);

                Process.Start(appPath);
            }
            catch (Exception) { }
        }

        private static void UpdateLang()
        {
            string langPath = Path.GetDirectoryName(appPath) + "\\lang\\";

            if (!Directory.Exists(langPath))
                return;

            try
            {
                bool langUpdateAvailable = false;

                foreach (var filePath in Directory.EnumerateFiles(langPath, "*.xml", SearchOption.TopDirectoryOnly))
                {
                    string fileName = Path.GetFileName(filePath);

                    if (!langUpdateAvailable)
                    {
                        try
                        {
                            string webXmlContent = webClient.DownloadString(repositoryURL + "ItemGen/Resources/lang/" + fileName);

                            if (File.ReadAllText(langPath + fileName) != webXmlContent)
                            {
                                langUpdateAvailable = true;
                                File.WriteAllText(langPath + fileName, webXmlContent);
                            }
                            else
                            {
                                return;
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show(errorMsg, appName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    else
                    {
                        try
                        {
                            webClient.DownloadFile(repositoryURL+"ItemGen/Resources/lang/" + fileName, langPath + fileName);
                        }
                        catch (WebException)
                        {
                            MessageBox.Show(errorMsg, appName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }
            }
            catch (Exception msg)
            {
                MessageBox.Show(msg.Message, appName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        static void Main(string[] args)
        {
            if (args.Length != 4)
                return;

            // Fix for SSL/TLS issue
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | (SecurityProtocolType)768 // Tls11
                   | (SecurityProtocolType)3072 // Tls12
                   | (SecurityProtocolType)12288 // Tls13
                   | (SecurityProtocolType)48; // SSL3

            webClient = new WebClient();

            appName = args[0];
            repositoryURL = args[1];
            appPath = args[2];
            errorMsg = args[3];

            if (!File.Exists(appPath))
                return;

            UpdateLang();
            UpdateExe();
        }
    }
}
