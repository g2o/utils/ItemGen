﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Windows.Forms;

namespace ItemGen
{
    class Lang
    {
        public static Dictionary<string, string> msg = new Dictionary<string, string>();

        public static void Load()
        {
            // ItemGen.cs

            // Interface
            msg.Add("INCLUDE_PATHS", "INCLUDE_PATHS");
            msg.Add("EXCLUDE_PATHS", "EXCLUDE_PATHS");
            msg.Add("SAVE_PATH", "SAVE_PATH");

            msg.Add("ADD", "ADD");
            msg.Add("REMOVE", "REMOVE");
            msg.Add("UPDATE", "UPDATE");
            msg.Add("GENERATE", "GENERATE");

            msg.Add("SELECT_FILES_TO_INCLUDE", "SELECT_FILES_TO_INCLUDE");
            msg.Add("SELECT_FILES_TO_EXCLUDE", "SELECT_FILES_TO_EXCLUDE");
            msg.Add("SAVE_XML_FILE", "SAVE_XML_FILE");
            msg.Add("PROGRESS", "PROGRESS");

            msg.Add("CURRENT_FILE", "CURRENT_FILE");

            msg.Add("ALL_FILES", "ALL_FILES");

            // Infos
            msg.Add("WOULD_YOU_LIKE_TO_UPDATE_TO_NEWER_VERSION", "WOULD_YOU_LIKE_TO_UPDATE_TO_NEWER_VERSION");

            // Alerts
            msg.Add("UPDATE_FAILED", "UPDATE_FAILED");
            msg.Add("UPDATE_NEEDS_ONLY_ONE_SELECTED_ITEM", "UPDATE_NEEDS_ONLY_ONE_SELECTED_ITEM");
            msg.Add("YOU_HAVE_TO_SPECIFY_SAVE_PATH", "YOU_HAVE_TO_SPECIFY_SAVE_PATH");
            msg.Add("YOU_HAVE_TO_INSERT_AT_LEAST_ONE_INCLUDE_PATH", "YOU_HAVE_TO_INSERT_AT_LEAST_ONE_INCLUDE_PATH");
            msg.Add("TASK_IS_ALREADY_EXECUTING_PLEASE_WAIT", "TASK_IS_ALREADY_EXECUTING_PLEASE_WAIT");

            // ToolTips
            msg.Add("SELECT_THE_SPECIFIC_FILE", "SELECT_THE_SPECIFIC_FILE");
            msg.Add("ADD_FILE_PATH_TO_THE_LIST", "ADD_FILE_PATH_TO_THE_LIST");
            msg.Add("REMOVE_FILE_PATH_FROM_THE_LIST", "REMOVE_FILE_PATH_FROM_THE_LIST");
            msg.Add("UPDATE_THE_SELECTED_PATH_IN_THE_LIST", "UPDATE_THE_SELECTED_PATH_IN_THE_LIST");
            msg.Add("GENERATE_THE_XML_FILE", "GENERATE_THE_XML_FILE");

            //ItemGen_About.cs
            msg.Add("AUTHORS", "AUTHORS");
            msg.Add("SOURCE_CODE", "SOURCE_CODE");
            msg.Add("CLICK_HERE", "CLICK_HERE");

            string fileName = "lang/" + Config.lang + ".xml";

            if (!Directory.Exists("lang"))
                return;

            if (!File.Exists(fileName))
            {
                if (Config.lang.Length != 0)
                    MessageBox.Show("Lang file "+ fileName + " doesn't exists!", Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            XDocument file;
            XElement root;

            try
            {
                file = XDocument.Load(fileName);
                root = file.Element("lang");

                if (root == null)
                    throw new Exception();
            }
            catch (Exception)
            {
                MessageBox.Show(fileName + " root <lang> is missing");
                return;
            }

            foreach (XElement node in root.Elements())
            {
                XAttribute attribute = node.Attribute("value");

                if (attribute == null)
                    continue;

                if (msg.ContainsKey(node.Name.LocalName))
                {
                    msg[node.Name.LocalName] = attribute.Value;
                }
            }
        }
    }
}
