﻿using System;
using System.Windows.Forms;
using System.IO;

namespace ItemGen
{
    public partial class ItemGen_LangSelect : Form
    {
        public ItemGen_LangSelect()
        {
            InitializeComponent();
        }

        public static bool isClosing = true;

        // Events

        private void ItemGen_LangSelect_Load(object sender, EventArgs e)
        {
            ItemGen_LangSelect_comboBox.Items.Add("none");

            string path = "lang\\";

            if (Directory.Exists(path))
            {
                foreach (string file in InputFilesList.GetFilesFromDir(path, "*.xml", System.IO.SearchOption.TopDirectoryOnly))
                {
                    ItemGen_LangSelect_comboBox.Items.Add(file.Substring(path.Length, file.Length - path.Length - 4));
                }
            }

            ItemGen_LangSelect_comboBox.SelectedIndex = 0;
        }

        private void ItemGen_LangSelect_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (isClosing)
                Application.Exit();
        }

        // Controls

        private void ItemGen_LangSelect_button_Click(object sender, EventArgs e)
        {
            isClosing = false;

            this.Close();

            string lang = ItemGen_LangSelect_comboBox.Items[ItemGen_LangSelect_comboBox.SelectedIndex].ToString();

            if (lang == "none")
                lang = "";

            Config.lang = lang;
        }
    }
}
