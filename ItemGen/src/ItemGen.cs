﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using System.IO;
using System.Media;
using System.Net;
using System.Reflection;
using System.Diagnostics;

namespace ItemGen
{
    public partial class ItemGen : Form
    {
        public ItemGen()
        {
            InitializeComponent();

            if (!ItemGen_LangSelect.isClosing || File.Exists("config.xml"))
                this.Visible = true;
        }

        // attributes

        private WebClient webClient;
        private string repositoryURL = "https://gitlab.com/Patrix9999/ItemGen/-/raw/master/";

        // Overrides

        protected override void SetVisibleCore(bool value)
        {
            if (!this.IsHandleCreated)
            {
                this.CreateHandle();
                value = false;
            }

            base.SetVisibleCore(value);
        }

        // Events

        private void ItemGen_Load(object sender, EventArgs e)
        {
            if (File.Exists("config.xml"))
            {
                Config.Load();

                save_textBox_path.Text = Config.savePath;

                for (int i = 0; i < Config.includePaths.Count; ++i)
                {
                    include_listBox_paths.Items.Add(Config.includePaths[i]);
                }

                for (int i = 0; i < Config.excludePaths.Count; ++i)
                {
                    exclude_listBox_paths.Items.Add(Config.excludePaths[i]);
                }
            }

            Lang.Load();

            // Interface
            include_groupBox.Text = Lang.msg["INCLUDE_PATHS"];
            exclude_groupBox.Text = Lang.msg["EXCLUDE_PATHS"];
            save_groupBox.Text = Lang.msg["SAVE_PATH"];
            progress_groupBox.Text = Lang.msg["PROGRESS"];

            include_button_add.Text = Lang.msg["ADD"];
            exclude_button_add.Text = Lang.msg["ADD"];

            include_button_remove.Text = Lang.msg["REMOVE"];
            exclude_button_remove.Text = Lang.msg["REMOVE"];

            include_button_update.Text = Lang.msg["UPDATE"];
            exclude_button_update.Text = Lang.msg["UPDATE"];

            save_button_generate.Text = Lang.msg["GENERATE"];

            // AutoUpdate
            if (Config.checkForUpdates == "true")
            {
                // Fix for SSL/TLS issue
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | (SecurityProtocolType)768 // Tls11
                       | (SecurityProtocolType)3072 // Tls12
                       | (SecurityProtocolType)12288 // Tls13
                       | (SecurityProtocolType)48; // SSL3

                webClient = new WebClient();

                webClient.Proxy = null;

                webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_downloadStringCompleted);
                webClient.DownloadStringAsync(new Uri(repositoryURL + "ItemGen/bin/Release/checksum"));
            }

            // ToolTips
            new ToolTip().SetToolTip(this.include_button_path, Lang.msg["SELECT_THE_SPECIFIC_FILE"]);

            new ToolTip().SetToolTip(this.include_button_add, Lang.msg["ADD_FILE_PATH_TO_THE_LIST"]);
            new ToolTip().SetToolTip(this.include_button_remove, Lang.msg["REMOVE_FILE_PATH_FROM_THE_LIST"]);
            new ToolTip().SetToolTip(this.include_button_update, Lang.msg["UPDATE_THE_SELECTED_PATH_IN_THE_LIST"]);

            new ToolTip().SetToolTip(this.exclude_button_path, Lang.msg["SELECT_THE_SPECIFIC_FILE"]);

            new ToolTip().SetToolTip(this.exclude_button_add, Lang.msg["ADD_FILE_PATH_TO_THE_LIST"]);
            new ToolTip().SetToolTip(this.exclude_button_remove, Lang.msg["REMOVE_FILE_PATH_FROM_THE_LIST"]);
            new ToolTip().SetToolTip(this.exclude_button_update, Lang.msg["UPDATE_THE_SELECTED_PATH_IN_THE_LIST"]);

            new ToolTip().SetToolTip(this.save_button_path, Lang.msg["SELECT_THE_SPECIFIC_FILE"]);
            new ToolTip().SetToolTip(this.save_button_generate, Lang.msg["GENERATE_THE_XML_FILE"]);
        }

        private void ItemGen_FormClosing(object sender, FormClosingEventArgs e)
        {
            Config.savePath = save_textBox_path.Text;
            
            Config.Save();
        }

        private void webClient_downloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.ToString(), Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string serverHash = e.Result;
            string clientHash = FileChecksum.computeHash("SHA1", Assembly.GetExecutingAssembly().Location);

            if (clientHash == serverHash)
                return;

            if (MessageBox.Show(Lang.msg["WOULD_YOU_LIKE_TO_UPDATE_TO_NEWER_VERSION"], Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) != DialogResult.OK)
                return;

            ProcessStartInfo info = new ProcessStartInfo();

            info.Arguments = "\"" + Application.OpenForms["ItemGen"].Text + "\" \"" + repositoryURL + "\" \"" + Assembly.GetExecutingAssembly().Location + "\" \""+ Lang.msg["UPDATE_FAILED"] + "\"";
            info.WindowStyle = ProcessWindowStyle.Hidden;
            info.CreateNoWindow = true;
            info.FileName = "updater.exe";
            info.Verb = "runas";

            try
            {
                Process.Start(info);
                Process.GetProcessById(Process.GetCurrentProcess().Id).Kill();
            }
            catch (Exception) { }
        }

        // Toolstrip Controls

        private void ItemGen_toolStrip_About_Click(object sender, EventArgs e)
        {
            ItemGen_About form = new ItemGen_About();
            form.ShowDialog();
        }

        //Include Controls

        private void include_textBox_path_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void include_textBox_path_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            if (files != null && files.Length != 0)
                include_textBox_path.Text = files[0];
        }

        private void include_button_path_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter =
                "Daedalus (*.d)|*.d|" +
                 Lang.msg["ALL_FILES"] + " (*.*)|*.*";
            openFileDialog.Title = Lang.msg["SELECT_FILES_TO_INCLUDE"];

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foreach(string fileName in openFileDialog.FileNames)
                    include_listBox_paths.Items.Add(fileName);
            }
        }

        private void include_button_add_Click(object sender, EventArgs e)
        {
            var path = include_textBox_path.Text;

            if (path.Length > 0)
               include_listBox_paths.Items.Add(path);
        }

        private void include_button_remove_Click(object sender, EventArgs e)
        {
            for (int i = include_listBox_paths.SelectedIndices.Count - 1; i >= 0; --i)
            {
                include_listBox_paths.Items.RemoveAt(i);
            }
        }

        private void include_button_update_Click(object sender, EventArgs e)
        {
            if (include_listBox_paths.SelectedIndices.Count > 1)
            {
                MessageBox.Show(Lang.msg["UPDATE_NEEDS_ONLY_ONE_SELECTED_ITEM"], Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var path = include_textBox_path.Text;

            if(path.Length < 1)
                return;

            int index = include_listBox_paths.SelectedIndices[0];

            include_listBox_paths.Items.RemoveAt(index);
            include_listBox_paths.Items.Insert(index, path);
        }

        private void include_listBox_paths_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (include_listBox_paths.SelectedIndices.Count != 1)
                return;

            include_textBox_path.Text = include_listBox_paths.SelectedItem.ToString();
        }

        private void include_listBox_paths_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void include_listBox_paths_DragDrop(object sender, DragEventArgs e)
        {
            string[] elements = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            foreach (string item in elements)
            {
                if (File.Exists(item))
                {
                    include_listBox_paths.Items.Add(item);
                }
                else if (Directory.Exists(item))
                {
                    include_listBox_paths.Items.Add(item + "\\*");
                }
            }
        }

        //Exclude Controls

        private void exclude_textBox_path_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void exclude_textBox_path_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            if (files != null && files.Length != 0)
                exclude_textBox_path.Text = files[0];
        }

        private void exclude_button_path_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter =
                "Daedalus (*.d)|*.d|" +
                Lang.msg["ALL_FILES"] + " (*.*)|*.*";
            openFileDialog.Title = Lang.msg["SELECT_FILES_TO_EXCLUDE"];

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foreach (string fileName in openFileDialog.FileNames)
                    exclude_listBox_paths.Items.Add(fileName);
            }
        }

        private void exclude_button_add_Click(object sender, EventArgs e)
        {
            var path = exclude_textBox_path.Text;

            if (path.Length > 0)
                exclude_listBox_paths.Items.Add(path);
        }

        private void exclude_button_remove_Click(object sender, EventArgs e)
        {
            for (int i = exclude_listBox_paths.SelectedIndices.Count - 1; i >= 0; --i)
            {
                exclude_listBox_paths.Items.RemoveAt(i);
            }
        }

        private void exclude_button_update_Click(object sender, EventArgs e)
        {
            if (exclude_listBox_paths.SelectedIndices.Count > 1)
            {
                MessageBox.Show(Lang.msg["UPDATE_NEEDS_ONLY_ONE_SELECTED_ITEM"], Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var path = exclude_textBox_path.Text;

            if (path.Length < 1)
                return;

            int index = exclude_listBox_paths.SelectedIndices[0];

            exclude_listBox_paths.Items.RemoveAt(index);
            exclude_listBox_paths.Items.Insert(index, path);
        }

        private void exclude_listBox_paths_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (exclude_listBox_paths.SelectedIndices.Count != 1)
                return;

            exclude_textBox_path.Text = exclude_listBox_paths.SelectedItem.ToString();
        }

        private void exclude_listBox_paths_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void exclude_listBox_paths_DragDrop(object sender, DragEventArgs e)
        {
            string[] elements = (string[]) e.Data.GetData(DataFormats.FileDrop, false);

            foreach (string item in elements)
            {
                if (File.Exists(item))
                {
                    exclude_listBox_paths.Items.Add(item);
                }
                else if (Directory.Exists(item))
                {
                    exclude_listBox_paths.Items.Add(item + "\\");
                }
            }
        }

        // backgroundWorker
        float currentProcessFile = 0;
        int filesCount = 0;

        private void ItemGen_backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            List<string> files = InputFilesList.GetFiles(include_listBox_paths.Items.Cast<string>().ToList(), exclude_listBox_paths.Items.Cast<string>().ToList());
            filesCount = files.Count;

            foreach (var file in files)
            {
                ++currentProcessFile;

                if (this.ItemGen_backgroundWorker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                Parser.Process(file);

                ItemGen_backgroundWorker.ReportProgress((int) Math.Round((currentProcessFile / files.Count) * 100, 2));
            }
        }

        private void ItemGen_backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progress_progressbar.Value = e.ProgressPercentage;
            progress_groupBox.Text = Lang.msg["CURRENT_FILE"] + ": " + currentProcessFile + "/" + filesCount;
        }

        private void ItemGen_backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                return;
            }
            else if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            currentProcessFile = 0;

            progress_groupBox.Text = Lang.msg["PROGRESS"];

            XMLGenerator.generate(save_textBox_path.Text);

            Parser.processedInstances.Clear();
            Parser.instances.Clear();

            Parser.prototypes.Clear();

            SystemSounds.Asterisk.Play();
            WindowsFlash.FlashWindowEx(this);
        }

        //Save Controls

        private void save_textBox_path_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void save_textBox_path_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            if (files != null && files.Length != 0)
                save_textBox_path.Text = files[0];
        }

        private void save_button_path_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XML (*.xml)|*.xml";
            saveFileDialog.Title = Lang.msg["SAVE_XML_FILE"];
            saveFileDialog.FileName = "items.xml";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                save_textBox_path.Text = saveFileDialog.FileName;
            }
        }

        private void save_button_generate_Click(object sender, EventArgs e)
        {
            if (include_listBox_paths.Items.Count == 0)
            {
                MessageBox.Show(Lang.msg["YOU_HAVE_TO_INSERT_AT_LEAST_ONE_INCLUDE_PATH"], Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (save_textBox_path.Text.Length == 0)
            {
                MessageBox.Show(Lang.msg["YOU_HAVE_TO_SPECIFY_SAVE_PATH"], Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (ItemGen_backgroundWorker.IsBusy)
            {
                MessageBox.Show(Lang.msg["TASK_IS_ALREADY_EXECUTING_PLEASE_WAIT"], Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            ItemGen_backgroundWorker.RunWorkerAsync();
        }
    }
}
