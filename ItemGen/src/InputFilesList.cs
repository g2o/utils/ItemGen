﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace ItemGen
{
    internal class InputFilesList
    {
        public static List<string> GetFilesFromDir(string path, string searchPattern, SearchOption searchOption = SearchOption.AllDirectories)
        {
            if (path.Length == 0)
                path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var result = new List<string>();

            try
            {
                foreach (var file in Directory.EnumerateFiles(path, searchPattern, searchOption))
                {
                    result.Add(file);
                }

                return result;
            }
            catch(Exception msg)
            {
                MessageBox.Show(msg.Message, Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return new List<string>();
        }

        public static IEnumerable<string> GetFilesFromDirs(IEnumerable<string> includeFiles, List<string> excludedFiles, SearchOption searchOption = SearchOption.AllDirectories)
        {
            foreach (var file in includeFiles)
            {
                if (excludedFiles.Contains(file))
                {
                    continue;
                }

                yield return file;
            }
        }

        public static List<string> GetFiles(List<string> includePaths, List<string> excludePaths)
        {
            var result = new List<string>();

            List<string> includeFiles = new List<string>();

            foreach (var path in includePaths)
            {
                if (!File.Exists(path))
                {
                    var searchPattern = Path.GetFileName(path);
                    string directory = path.Substring(0, path.Length - searchPattern.Length);

                    if (searchPattern == "")
                        searchPattern = "*";

                    foreach (string file in GetFilesFromDir(directory, searchPattern))
                        includeFiles.Add(file);
                }
                else
                    includeFiles.Add(path);
            }

            List<string> excludeFiles = new List<string>();

            foreach (var path in excludePaths)
            {
                if (!File.Exists(path))
                {
                    string searchPattern = Path.GetFileName(path);
                    string directory = path.Substring(0, path.Length - searchPattern.Length);

                    if (searchPattern == "")
                        searchPattern = "*";

                    var files = GetFilesFromDir(directory, searchPattern);

                    foreach (string file in GetFilesFromDir(directory, searchPattern))
                        excludeFiles.Add(file);
                }
                else
                    excludeFiles.Add(path);
            }

            foreach (string file in GetFilesFromDirs(includeFiles, excludeFiles))
                result.Add(file);

            return result;
        }
    }
}
