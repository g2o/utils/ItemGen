﻿namespace ItemGen
{
    partial class ItemGen_LangSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ItemGen_LangSelect_label = new System.Windows.Forms.Label();
            this.ItemGen_LangSelect_comboBox = new System.Windows.Forms.ComboBox();
            this.ItemGen_LangSelect_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ItemGen_LangSelect_label
            // 
            this.ItemGen_LangSelect_label.AutoSize = true;
            this.ItemGen_LangSelect_label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ItemGen_LangSelect_label.Location = new System.Drawing.Point(34, 9);
            this.ItemGen_LangSelect_label.Name = "ItemGen_LangSelect_label";
            this.ItemGen_LangSelect_label.Size = new System.Drawing.Size(141, 16);
            this.ItemGen_LangSelect_label.TabIndex = 0;
            this.ItemGen_LangSelect_label.Text = "Choose your language:";
            // 
            // ItemGen_LangSelect_comboBox
            // 
            this.ItemGen_LangSelect_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ItemGen_LangSelect_comboBox.FormattingEnabled = true;
            this.ItemGen_LangSelect_comboBox.Location = new System.Drawing.Point(12, 36);
            this.ItemGen_LangSelect_comboBox.Name = "ItemGen_LangSelect_comboBox";
            this.ItemGen_LangSelect_comboBox.Size = new System.Drawing.Size(199, 22);
            this.ItemGen_LangSelect_comboBox.TabIndex = 1;
            // 
            // ItemGen_LangSelect_button
            // 
            this.ItemGen_LangSelect_button.Font = new System.Drawing.Font("Arial", 9F);
            this.ItemGen_LangSelect_button.Location = new System.Drawing.Point(70, 63);
            this.ItemGen_LangSelect_button.Name = "ItemGen_LangSelect_button";
            this.ItemGen_LangSelect_button.Size = new System.Drawing.Size(77, 23);
            this.ItemGen_LangSelect_button.TabIndex = 2;
            this.ItemGen_LangSelect_button.Text = "Accept";
            this.ItemGen_LangSelect_button.UseVisualStyleBackColor = true;
            this.ItemGen_LangSelect_button.Click += new System.EventHandler(this.ItemGen_LangSelect_button_Click);
            // 
            // ItemGen_LangSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(223, 98);
            this.Controls.Add(this.ItemGen_LangSelect_button);
            this.Controls.Add(this.ItemGen_LangSelect_comboBox);
            this.Controls.Add(this.ItemGen_LangSelect_label);
            this.Font = new System.Drawing.Font("Arial", 8F);
            this.Name = "ItemGen_LangSelect";
            this.ShowIcon = false;
            this.Text = "ItemGen";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ItemGen_LangSelect_FormClosed);
            this.Load += new System.EventHandler(this.ItemGen_LangSelect_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ItemGen_LangSelect_label;
        private System.Windows.Forms.ComboBox ItemGen_LangSelect_comboBox;
        private System.Windows.Forms.Button ItemGen_LangSelect_button;
    }
}