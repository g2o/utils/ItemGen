﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ItemGen
{
    internal class Parser
    {
        static int insideComment = 0;
        static int commentFoundChars = 0;
        
        static bool insideString = false;

        static string[] startKeywords = {"instance", "prototype"};
        static string[] unexpectedKeywords = {"instance", "prototype", "class", "func"};

        static string foundKeyword = "";
        static int keyWordIndex = -1;
        static int keyWordFoundChars = 0;

        private enum operation:uint
        {
            findKeyword,
            readItemName,
            readItemBody,
        }

        public sealed class Item
        {
            public string instance;

            public Item(string instanceName)
            {
                instance = instanceName;
            }
        }

        public static Dictionary<string, Item> prototypes = new Dictionary<string, Item>();
        public static HashSet<string> instances = new HashSet<string>();
        public static List<Item> processedInstances = new List<Item>();

        static uint currentOperation = (uint)operation.findKeyword;
        static string buffor = "";

        public static void ProcessComments(char ch)
        {
            if (insideComment == 0)
            {
                if (commentFoundChars == 0 && ch == '/')
                {
                    ++commentFoundChars;
                }
                else if (commentFoundChars == 1)
                {
                    if (ch == '/')
                    {
                        insideComment = 1;
                    }
                    else if (ch == '*')
                    {
                        insideComment = 2;
                    }

                    commentFoundChars = 0;

                    if (buffor.Length >= 2)
                        buffor = buffor.Remove(buffor.Length - 2);
                }
            }
            else if (insideComment == 1 && ch == '\n')
            {
                insideComment = 0;
            }
            else if (insideComment == 2)
            {
                if (commentFoundChars == 0 && ch == '*')
                {
                    ++commentFoundChars;
                }
                else if (commentFoundChars == 1)
                {
                    if (ch == '/')
                    {
                        insideComment = 0;
                    }

                    commentFoundChars = 0;
                }
            }
        }

        public static void ProcesString(char ch)
        {
            if (ch == '"')
            {
                if (buffor.Length != 0)
                    buffor = buffor.Remove(buffor.Length - 1);

                insideString = !insideString;
            }
        }

        public static string FindKeyword(char ch, string[] keywords)
        {
            if (keyWordFoundChars == 0)
            {
                for (int i = 0; i < keywords.Length; ++i)
                {
                    if (ch == keywords[i][0])
                    {
                        keyWordIndex = i;
                        ++keyWordFoundChars;
                        break;
                    }
                }
            }
            else
            {
                if (keyWordFoundChars == keywords[keyWordIndex].Length)
                {
                    keyWordFoundChars = 0;

                    if (Char.IsWhiteSpace(ch))
                    {
                        int index = keyWordIndex;
                        keyWordIndex = -1;

                        return keywords[index];
                    }
                }
               else if (ch == keywords[keyWordIndex][keyWordFoundChars])
                {
                    ++keyWordFoundChars;
                }
                else
                {
                    keyWordFoundChars = 0;
                }
            }

            return "";     
        }

        // Read methods

        public static void ProcessPrototypes(char ch)
        {
            if (insideComment == 0)
            {
                if (!insideString)
                {
                    ch = Char.ToLower(ch);

                    switch (currentOperation)
                    {
                        case (uint)operation.readItemName:
                            if (ch == '{')
                            {
                                currentOperation = (uint)operation.readItemBody;

                                int beginPos = buffor.LastIndexOf('(') + 1;
                                string className = buffor.Substring(beginPos, buffor.Length - beginPos - 1);

                                if (className == "c_item" || prototypes.ContainsKey(className))
                                {
                                    buffor = buffor.Substring(0, buffor.Length - className.Length - 2).ToUpper();

                                    if (!prototypes.ContainsKey(buffor))
                                        prototypes[buffor] = new Item("");
                                }

                                buffor = "";
                            }
                            else if (!Char.IsWhiteSpace(ch))
                            {
                                buffor += ch;
                            }
                            else if (ch == ';' || FindKeyword(ch, unexpectedKeywords) != "")
                            { // Clearing buffor, if something wasn't read as it was expected
                                currentOperation = (uint)operation.findKeyword;

                                foundKeyword = "";
                                buffor = "";
                            }

                            break;

                        case (uint)operation.readItemBody:
                            if (ch == '}')
                            {
                                currentOperation = (uint)operation.findKeyword;

                                foundKeyword = "";
                                buffor = "";
                            }
                            else if (ch == ';')
                            { // section added for future development, currently not used
                                buffor = "";
                            }
                            else if (!Char.IsWhiteSpace(ch))
                            {
                                buffor += ch;
                            }
                            else if (FindKeyword(ch, unexpectedKeywords) != "")
                            { // Clearing buffor, if something wasn't read as it was expected
                                currentOperation = (uint)operation.findKeyword;

                                foundKeyword = "";
                                buffor = "";
                            }
                            break;
                    }
                }
                else
                {
                    if (currentOperation == (uint)operation.readItemBody)
                    {
                        buffor += ch;
                    }
                }
            }
        }

        public static void ProcessInstances(char ch)
        {
            if (insideComment == 0)
            {
                if (!insideString)
                {
                    ch = Char.ToLower(ch);

                    switch (currentOperation)
                    {
                        case (uint)operation.readItemName:
                            if (ch == '{')
                            {
                                currentOperation = (uint)operation.readItemBody;

                                int beginPos = buffor.LastIndexOf('(') + 1;
                                string className = buffor.Substring(beginPos, buffor.Length - beginPos - 1);

                                if (className == "c_item" || prototypes.ContainsKey(className.ToUpper()))
                                {
                                    buffor = buffor.Substring(0, buffor.Length - className.Length - 2).ToUpper();

                                    if (!instances.Contains(buffor))
                                    {
                                        instances.Add(buffor);
                                        processedInstances.Add(new Item(buffor));
                                    }
                                }

                                buffor = "";
                            }
                            else if (!Char.IsWhiteSpace(ch))
                            {
                                buffor += ch;
                            }
                            else if (ch == ';' || FindKeyword(ch, unexpectedKeywords) != "")
                            { // Clearing buffor, if something wasn't read as it was expected
                                currentOperation = (uint)operation.findKeyword;

                                foundKeyword = "";
                                buffor = "";
                            }

                            break;

                        case (uint)operation.readItemBody:
                            if (ch == '}')
                            {
                                currentOperation = (uint)operation.findKeyword;

                                foundKeyword = "";
                                buffor = "";
                            }
                            else if (ch == ';')
                            { // Section added for future development, currently not used
                                buffor = "";
                            }
                            else if (!Char.IsWhiteSpace(ch))
                            {
                                buffor += ch;
                            }
                            else if (FindKeyword(ch, unexpectedKeywords) != "")
                            { // Clearing buffor, if something wasn't read as it was expected
                                currentOperation = (uint)operation.findKeyword;

                                foundKeyword = "";
                                buffor = "";
                            }
                            break;
                    }
                }
                else
                {
                    if (currentOperation == (uint)operation.readItemBody)
                    {
                        buffor += ch;
                    }
                }
            }
        }

        public static void Process(string fileName)
        {
            StreamReader file = new StreamReader(fileName);

            do
            {
                char ch = (char) file.Read();

                if (foundKeyword == "")
                {
                    foundKeyword = FindKeyword(Char.ToLower(ch), startKeywords);
                    currentOperation = (uint)operation.readItemName;
                }

                switch (foundKeyword)
                {
                    case "prototype":
                        ProcessPrototypes(ch);
                        break;

                    case "instance":
                        ProcessInstances(ch);
                        break;
                }

                if (insideString == false)
                {
                    ProcessComments(ch);
                }

                if (insideComment == 0)
                {
                    ProcesString(ch);
                }
            }
            while (!file.EndOfStream);
            
            file.Close();

            //Restore default values
            insideComment = 0;
            commentFoundChars = 0;

            insideString = false;

            foundKeyword = "";
            keyWordIndex = -1;
            keyWordFoundChars = 0;

            currentOperation = (uint)operation.findKeyword;

            buffor = "";
        }
    }
}
