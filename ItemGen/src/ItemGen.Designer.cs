﻿namespace ItemGen
{
    partial class ItemGen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemGen));
            this.include_listBox_paths = new System.Windows.Forms.ListBox();
            this.include_button_add = new System.Windows.Forms.Button();
            this.include_button_remove = new System.Windows.Forms.Button();
            this.include_button_update = new System.Windows.Forms.Button();
            this.include_groupBox = new System.Windows.Forms.GroupBox();
            this.include_button_path = new System.Windows.Forms.Button();
            this.include_textBox_path = new System.Windows.Forms.TextBox();
            this.exclude_groupBox = new System.Windows.Forms.GroupBox();
            this.exclude_button_path = new System.Windows.Forms.Button();
            this.exclude_listBox_paths = new System.Windows.Forms.ListBox();
            this.exclude_button_remove = new System.Windows.Forms.Button();
            this.exclude_textBox_path = new System.Windows.Forms.TextBox();
            this.exclude_button_update = new System.Windows.Forms.Button();
            this.exclude_button_add = new System.Windows.Forms.Button();
            this.save_button_path = new System.Windows.Forms.Button();
            this.save_textBox_path = new System.Windows.Forms.TextBox();
            this.save_button_generate = new System.Windows.Forms.Button();
            this.save_groupBox = new System.Windows.Forms.GroupBox();
            this.ItemGen_toolStrip = new System.Windows.Forms.ToolStrip();
            this.ItemGen_toolStrip_About = new System.Windows.Forms.ToolStripButton();
            this.ItemGen_backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.progress_groupBox = new System.Windows.Forms.GroupBox();
            this.progress_progressbar = new System.Windows.Forms.ProgressBar();
            this.include_groupBox.SuspendLayout();
            this.exclude_groupBox.SuspendLayout();
            this.save_groupBox.SuspendLayout();
            this.ItemGen_toolStrip.SuspendLayout();
            this.progress_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // include_listBox_paths
            // 
            this.include_listBox_paths.AllowDrop = true;
            this.include_listBox_paths.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.include_listBox_paths.FormattingEnabled = true;
            this.include_listBox_paths.HorizontalScrollbar = true;
            this.include_listBox_paths.ItemHeight = 16;
            this.include_listBox_paths.Location = new System.Drawing.Point(6, 52);
            this.include_listBox_paths.Name = "include_listBox_paths";
            this.include_listBox_paths.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.include_listBox_paths.Size = new System.Drawing.Size(226, 116);
            this.include_listBox_paths.TabIndex = 3;
            this.include_listBox_paths.SelectedIndexChanged += new System.EventHandler(this.include_listBox_paths_SelectedIndexChanged);
            this.include_listBox_paths.DragDrop += new System.Windows.Forms.DragEventHandler(this.include_listBox_paths_DragDrop);
            this.include_listBox_paths.DragEnter += new System.Windows.Forms.DragEventHandler(this.include_listBox_paths_DragEnter);
            // 
            // include_button_add
            // 
            this.include_button_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.include_button_add.Location = new System.Drawing.Point(238, 51);
            this.include_button_add.Name = "include_button_add";
            this.include_button_add.Size = new System.Drawing.Size(89, 34);
            this.include_button_add.TabIndex = 4;
            this.include_button_add.Text = "Add";
            this.include_button_add.UseVisualStyleBackColor = true;
            this.include_button_add.Click += new System.EventHandler(this.include_button_add_Click);
            // 
            // include_button_remove
            // 
            this.include_button_remove.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.include_button_remove.Location = new System.Drawing.Point(238, 93);
            this.include_button_remove.Name = "include_button_remove";
            this.include_button_remove.Size = new System.Drawing.Size(89, 34);
            this.include_button_remove.TabIndex = 5;
            this.include_button_remove.Text = "Remove";
            this.include_button_remove.UseVisualStyleBackColor = true;
            this.include_button_remove.Click += new System.EventHandler(this.include_button_remove_Click);
            // 
            // include_button_update
            // 
            this.include_button_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.include_button_update.Location = new System.Drawing.Point(238, 135);
            this.include_button_update.Name = "include_button_update";
            this.include_button_update.Size = new System.Drawing.Size(89, 34);
            this.include_button_update.TabIndex = 6;
            this.include_button_update.Text = "Update";
            this.include_button_update.UseVisualStyleBackColor = true;
            this.include_button_update.Click += new System.EventHandler(this.include_button_update_Click);
            // 
            // include_groupBox
            // 
            this.include_groupBox.Controls.Add(this.include_button_path);
            this.include_groupBox.Controls.Add(this.include_listBox_paths);
            this.include_groupBox.Controls.Add(this.include_button_remove);
            this.include_groupBox.Controls.Add(this.include_textBox_path);
            this.include_groupBox.Controls.Add(this.include_button_update);
            this.include_groupBox.Controls.Add(this.include_button_add);
            this.include_groupBox.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.include_groupBox.Location = new System.Drawing.Point(12, 28);
            this.include_groupBox.Name = "include_groupBox";
            this.include_groupBox.Size = new System.Drawing.Size(333, 176);
            this.include_groupBox.TabIndex = 7;
            this.include_groupBox.TabStop = false;
            this.include_groupBox.Text = "Include path(s)";
            // 
            // include_button_path
            // 
            this.include_button_path.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.include_button_path.Location = new System.Drawing.Point(296, 19);
            this.include_button_path.Name = "include_button_path";
            this.include_button_path.Size = new System.Drawing.Size(31, 23);
            this.include_button_path.TabIndex = 2;
            this.include_button_path.Text = "...";
            this.include_button_path.UseVisualStyleBackColor = true;
            this.include_button_path.Click += new System.EventHandler(this.include_button_path_Click);
            // 
            // include_textBox_path
            // 
            this.include_textBox_path.AllowDrop = true;
            this.include_textBox_path.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.include_textBox_path.Location = new System.Drawing.Point(6, 20);
            this.include_textBox_path.Name = "include_textBox_path";
            this.include_textBox_path.Size = new System.Drawing.Size(284, 20);
            this.include_textBox_path.TabIndex = 1;
            this.include_textBox_path.DragDrop += new System.Windows.Forms.DragEventHandler(this.include_textBox_path_DragDrop);
            this.include_textBox_path.DragEnter += new System.Windows.Forms.DragEventHandler(this.include_textBox_path_DragEnter);
            // 
            // exclude_groupBox
            // 
            this.exclude_groupBox.Controls.Add(this.exclude_button_path);
            this.exclude_groupBox.Controls.Add(this.exclude_listBox_paths);
            this.exclude_groupBox.Controls.Add(this.exclude_button_remove);
            this.exclude_groupBox.Controls.Add(this.exclude_textBox_path);
            this.exclude_groupBox.Controls.Add(this.exclude_button_update);
            this.exclude_groupBox.Controls.Add(this.exclude_button_add);
            this.exclude_groupBox.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exclude_groupBox.Location = new System.Drawing.Point(12, 210);
            this.exclude_groupBox.Name = "exclude_groupBox";
            this.exclude_groupBox.Size = new System.Drawing.Size(333, 176);
            this.exclude_groupBox.TabIndex = 8;
            this.exclude_groupBox.TabStop = false;
            this.exclude_groupBox.Text = "Exclude path(s)";
            // 
            // exclude_button_path
            // 
            this.exclude_button_path.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exclude_button_path.Location = new System.Drawing.Point(296, 19);
            this.exclude_button_path.Name = "exclude_button_path";
            this.exclude_button_path.Size = new System.Drawing.Size(31, 23);
            this.exclude_button_path.TabIndex = 8;
            this.exclude_button_path.Text = "...";
            this.exclude_button_path.UseVisualStyleBackColor = true;
            this.exclude_button_path.Click += new System.EventHandler(this.exclude_button_path_Click);
            // 
            // exclude_listBox_paths
            // 
            this.exclude_listBox_paths.AllowDrop = true;
            this.exclude_listBox_paths.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exclude_listBox_paths.FormattingEnabled = true;
            this.exclude_listBox_paths.HorizontalScrollbar = true;
            this.exclude_listBox_paths.ItemHeight = 16;
            this.exclude_listBox_paths.Location = new System.Drawing.Point(6, 52);
            this.exclude_listBox_paths.Name = "exclude_listBox_paths";
            this.exclude_listBox_paths.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.exclude_listBox_paths.Size = new System.Drawing.Size(226, 116);
            this.exclude_listBox_paths.TabIndex = 9;
            this.exclude_listBox_paths.SelectedIndexChanged += new System.EventHandler(this.exclude_listBox_paths_SelectedIndexChanged);
            this.exclude_listBox_paths.DragDrop += new System.Windows.Forms.DragEventHandler(this.exclude_listBox_paths_DragDrop);
            this.exclude_listBox_paths.DragEnter += new System.Windows.Forms.DragEventHandler(this.exclude_listBox_paths_DragEnter);
            // 
            // exclude_button_remove
            // 
            this.exclude_button_remove.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exclude_button_remove.Location = new System.Drawing.Point(238, 93);
            this.exclude_button_remove.Name = "exclude_button_remove";
            this.exclude_button_remove.Size = new System.Drawing.Size(89, 34);
            this.exclude_button_remove.TabIndex = 11;
            this.exclude_button_remove.Text = "Remove";
            this.exclude_button_remove.UseVisualStyleBackColor = true;
            this.exclude_button_remove.Click += new System.EventHandler(this.exclude_button_remove_Click);
            // 
            // exclude_textBox_path
            // 
            this.exclude_textBox_path.AllowDrop = true;
            this.exclude_textBox_path.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.exclude_textBox_path.Location = new System.Drawing.Point(6, 20);
            this.exclude_textBox_path.Name = "exclude_textBox_path";
            this.exclude_textBox_path.Size = new System.Drawing.Size(284, 20);
            this.exclude_textBox_path.TabIndex = 7;
            this.exclude_textBox_path.DragDrop += new System.Windows.Forms.DragEventHandler(this.exclude_textBox_path_DragDrop);
            this.exclude_textBox_path.DragEnter += new System.Windows.Forms.DragEventHandler(this.exclude_textBox_path_DragEnter);
            // 
            // exclude_button_update
            // 
            this.exclude_button_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exclude_button_update.Location = new System.Drawing.Point(238, 135);
            this.exclude_button_update.Name = "exclude_button_update";
            this.exclude_button_update.Size = new System.Drawing.Size(89, 34);
            this.exclude_button_update.TabIndex = 12;
            this.exclude_button_update.Text = "Update";
            this.exclude_button_update.UseVisualStyleBackColor = true;
            this.exclude_button_update.Click += new System.EventHandler(this.exclude_button_update_Click);
            // 
            // exclude_button_add
            // 
            this.exclude_button_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.exclude_button_add.Location = new System.Drawing.Point(238, 51);
            this.exclude_button_add.Name = "exclude_button_add";
            this.exclude_button_add.Size = new System.Drawing.Size(89, 34);
            this.exclude_button_add.TabIndex = 10;
            this.exclude_button_add.Text = "Add";
            this.exclude_button_add.UseVisualStyleBackColor = true;
            this.exclude_button_add.Click += new System.EventHandler(this.exclude_button_add_Click);
            // 
            // save_button_path
            // 
            this.save_button_path.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.save_button_path.Location = new System.Drawing.Point(296, 19);
            this.save_button_path.Name = "save_button_path";
            this.save_button_path.Size = new System.Drawing.Size(31, 22);
            this.save_button_path.TabIndex = 14;
            this.save_button_path.Text = "...";
            this.save_button_path.UseVisualStyleBackColor = true;
            this.save_button_path.Click += new System.EventHandler(this.save_button_path_Click);
            // 
            // save_textBox_path
            // 
            this.save_textBox_path.AllowDrop = true;
            this.save_textBox_path.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.save_textBox_path.Location = new System.Drawing.Point(6, 20);
            this.save_textBox_path.Name = "save_textBox_path";
            this.save_textBox_path.Size = new System.Drawing.Size(284, 20);
            this.save_textBox_path.TabIndex = 13;
            this.save_textBox_path.DragDrop += new System.Windows.Forms.DragEventHandler(this.save_textBox_path_DragDrop);
            this.save_textBox_path.DragEnter += new System.Windows.Forms.DragEventHandler(this.save_textBox_path_DragEnter);
            // 
            // save_button_generate
            // 
            this.save_button_generate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.save_button_generate.Location = new System.Drawing.Point(6, 52);
            this.save_button_generate.Name = "save_button_generate";
            this.save_button_generate.Size = new System.Drawing.Size(321, 34);
            this.save_button_generate.TabIndex = 15;
            this.save_button_generate.Text = "Generate";
            this.save_button_generate.UseVisualStyleBackColor = true;
            this.save_button_generate.Click += new System.EventHandler(this.save_button_generate_Click);
            // 
            // save_groupBox
            // 
            this.save_groupBox.Controls.Add(this.save_button_generate);
            this.save_groupBox.Controls.Add(this.save_button_path);
            this.save_groupBox.Controls.Add(this.save_textBox_path);
            this.save_groupBox.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.save_groupBox.Location = new System.Drawing.Point(12, 441);
            this.save_groupBox.Name = "save_groupBox";
            this.save_groupBox.Size = new System.Drawing.Size(333, 93);
            this.save_groupBox.TabIndex = 13;
            this.save_groupBox.TabStop = false;
            this.save_groupBox.Text = "Save path (.xml)";
            // 
            // ItemGen_toolStrip
            // 
            this.ItemGen_toolStrip.Font = new System.Drawing.Font("Arial", 10F);
            this.ItemGen_toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ItemGen_toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ItemGen_toolStrip_About});
            this.ItemGen_toolStrip.Location = new System.Drawing.Point(0, 0);
            this.ItemGen_toolStrip.Name = "ItemGen_toolStrip";
            this.ItemGen_toolStrip.Size = new System.Drawing.Size(359, 25);
            this.ItemGen_toolStrip.TabIndex = 14;
            this.ItemGen_toolStrip.Text = "ItemGen_toolStrip";
            // 
            // ItemGen_toolStrip_About
            // 
            this.ItemGen_toolStrip_About.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ItemGen_toolStrip_About.Font = new System.Drawing.Font("Arial", 10F);
            this.ItemGen_toolStrip_About.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ItemGen_toolStrip_About.Name = "ItemGen_toolStrip_About";
            this.ItemGen_toolStrip_About.Size = new System.Drawing.Size(35, 22);
            this.ItemGen_toolStrip_About.Text = "Info";
            this.ItemGen_toolStrip_About.Click += new System.EventHandler(this.ItemGen_toolStrip_About_Click);
            // 
            // ItemGen_backgroundWorker
            // 
            this.ItemGen_backgroundWorker.WorkerReportsProgress = true;
            this.ItemGen_backgroundWorker.WorkerSupportsCancellation = true;
            this.ItemGen_backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ItemGen_backgroundWorker_DoWork);
            this.ItemGen_backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.ItemGen_backgroundWorker_ProgressChanged);
            this.ItemGen_backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ItemGen_backgroundWorker_RunWorkerCompleted);
            // 
            // progress_groupBox
            // 
            this.progress_groupBox.Controls.Add(this.progress_progressbar);
            this.progress_groupBox.Font = new System.Drawing.Font("Arial", 10F);
            this.progress_groupBox.Location = new System.Drawing.Point(12, 392);
            this.progress_groupBox.Name = "progress_groupBox";
            this.progress_groupBox.Size = new System.Drawing.Size(333, 43);
            this.progress_groupBox.TabIndex = 15;
            this.progress_groupBox.TabStop = false;
            this.progress_groupBox.Text = "Progress";
            // 
            // progress_progressbar
            // 
            this.progress_progressbar.Location = new System.Drawing.Point(6, 18);
            this.progress_progressbar.Name = "progress_progressbar";
            this.progress_progressbar.Size = new System.Drawing.Size(315, 19);
            this.progress_progressbar.TabIndex = 0;
            // 
            // ItemGen
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(359, 538);
            this.Controls.Add(this.progress_groupBox);
            this.Controls.Add(this.ItemGen_toolStrip);
            this.Controls.Add(this.save_groupBox);
            this.Controls.Add(this.exclude_groupBox);
            this.Controls.Add(this.include_groupBox);
            this.Font = new System.Drawing.Font("Arial", 8F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ItemGen";
            this.Text = "ItemGen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ItemGen_FormClosing);
            this.Load += new System.EventHandler(this.ItemGen_Load);
            this.include_groupBox.ResumeLayout(false);
            this.include_groupBox.PerformLayout();
            this.exclude_groupBox.ResumeLayout(false);
            this.exclude_groupBox.PerformLayout();
            this.save_groupBox.ResumeLayout(false);
            this.save_groupBox.PerformLayout();
            this.ItemGen_toolStrip.ResumeLayout(false);
            this.ItemGen_toolStrip.PerformLayout();
            this.progress_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox include_listBox_paths;
        private System.Windows.Forms.Button include_button_add;
        private System.Windows.Forms.Button include_button_remove;
        private System.Windows.Forms.Button include_button_update;
        private System.Windows.Forms.GroupBox include_groupBox;
        private System.Windows.Forms.TextBox include_textBox_path;
        private System.Windows.Forms.Button include_button_path;
        private System.Windows.Forms.GroupBox exclude_groupBox;
        private System.Windows.Forms.Button exclude_button_path;
        private System.Windows.Forms.ListBox exclude_listBox_paths;
        private System.Windows.Forms.Button exclude_button_remove;
        private System.Windows.Forms.TextBox exclude_textBox_path;
        private System.Windows.Forms.Button exclude_button_update;
        private System.Windows.Forms.Button exclude_button_add;
        private System.Windows.Forms.Button save_button_path;
        private System.Windows.Forms.TextBox save_textBox_path;
        private System.Windows.Forms.Button save_button_generate;
        private System.Windows.Forms.GroupBox save_groupBox;
        private System.Windows.Forms.ToolStrip ItemGen_toolStrip;
        private System.Windows.Forms.ToolStripButton ItemGen_toolStrip_About;
        private System.ComponentModel.BackgroundWorker ItemGen_backgroundWorker;
        private System.Windows.Forms.GroupBox progress_groupBox;
        private System.Windows.Forms.ProgressBar progress_progressbar;
    }
}

