﻿using System.Collections.Generic;
using System.Xml;
using System.Windows.Forms;

namespace ItemGen
{
    class Config
    {
        public static string savePath = "";
        public static string checkForUpdates = "true";
        public static string lang = "";

        public static List<string> includePaths = new List<string>();
        public static List<string> excludePaths = new List<string>();

        private static XmlWriterSettings writerSettings = new XmlWriterSettings()
        {
            Indent = true,
            IndentChars = "\t",
            OmitXmlDeclaration = false,
        };

        public static void Save()
        {
            XmlWriter writer = XmlWriter.Create("config.xml", writerSettings);

            writer.WriteStartElement("config");

            writer.WriteStartElement("checkForUpdates");
            writer.WriteAttributeString("value", checkForUpdates);
            writer.WriteEndElement();

            writer.WriteStartElement("lang");
            writer.WriteAttributeString("value", lang);
            writer.WriteEndElement();

            writer.WriteStartElement("savePath");
            writer.WriteAttributeString("value", savePath);
            writer.WriteEndElement();

            // Optional includePaths save if they were specified by user

            if (includePaths.Count > 0)
            {
                writer.WriteStartElement("includePaths");

                for (int i = 0; i < includePaths.Count; ++i)
                {
                    writer.WriteStartElement("path");
                    writer.WriteValue(includePaths[i]);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();  
            }

            // Optional excludePaths save if they were specified by user

            if (excludePaths.Count > 0)
            {
                writer.WriteStartElement("excludePaths");

                for (int i = 0; i < excludePaths.Count; ++i)
                {
                    writer.WriteStartElement("path");
                    writer.WriteValue(excludePaths[i]);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();

            writer.Close();
        }

        public static void Load()
        {
            XmlDocument config = new XmlDocument();

            config.Load("config.xml");

            // getting root node

            XmlNode root = config.SelectSingleNode("config");

            if (root == null)
            {
                MessageBox.Show("Config.xml root <config> is missing", Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // reading lang node

            XmlNode node = root.SelectSingleNode("lang");

            if (node == null)
            {
                MessageBox.Show("Config.xml <lang> is missing", Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (node.Attributes == null || node.Attributes.GetNamedItem("value") == null)
            {
                MessageBox.Show("Config.xml <lang> doesn't have value attribute", Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            lang = node.Attributes["value"].Value;

            // reading autoUpdate node

            node = root.SelectSingleNode("checkForUpdates");

            if (node == null)
            {
                MessageBox.Show("Config.xml <checkForUpdates> is missing", Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (node.Attributes == null || node.Attributes.GetNamedItem("value") == null)
            {
                MessageBox.Show("Config.xml <checkForUpdates> doesn't have value attribute", Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            checkForUpdates = node.Attributes["value"].Value;

            // reading savePath node

            node = root.SelectSingleNode("savePath");

            if (node == null)
            {
                MessageBox.Show("Config.xml <savePath> is missing", Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (node.Attributes == null || node.Attributes.GetNamedItem("value") == null)
            {
                MessageBox.Show("Config.xml <savePath> doesn't have value attribute", Application.OpenForms["ItemGen"].Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            savePath = node.Attributes["value"].Value;

            // Optional reading includePaths nodes

            node = root.SelectSingleNode("includePaths");

            if (node == null)
                return;

            XmlNodeList pathNodes = node.SelectNodes("path");

            for (int i = 0; i < pathNodes.Count; i++)
            {
                includePaths.Add(pathNodes[i].InnerText);
            }

            // Optional reading excludePaths nodes

            node = root.SelectSingleNode("excludePaths");

            if (node == null)
                return;

            pathNodes = node.SelectNodes("path");

            for (int i = 0; i < pathNodes.Count; i++)
            {
                excludePaths.Add(pathNodes[i].InnerText);
            }
        }
    }
}
