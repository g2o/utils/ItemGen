﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;

namespace ItemGen
{
    public partial class ItemGen_About : Form
    {
        public ItemGen_About()
        {
            InitializeComponent();
        }

        // Events

        private void ItemGen_About_Load(object sender, EventArgs e)
        {
            ItemGen_About_label_Authors.Text = Lang.msg["AUTHORS"];
            ItemGen_About_label_sourceCode.Text = Lang.msg["SOURCE_CODE"];
            ItemGen_About_linkLabel_clickHere.Text = Lang.msg["CLICK_HERE"];
        }

        // Controls

        private void ItemGen_About_Link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://gitlab.com/Patrix9999");
        }
    }
}
