﻿using System;
using System.Windows.Forms;
using System.IO;

namespace ItemGen
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (!File.Exists("config.xml"))
            {
                ItemGen_LangSelect form = new ItemGen_LangSelect();
                form.ShowDialog();
            }

            Application.Run(new ItemGen());
            
        }
    }
}
