﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Xml;

namespace ItemGen
{
    class XMLGenerator
    {
        private static XmlWriterSettings settings = new XmlWriterSettings()
        {
            Indent = true,
            IndentChars = "\t",
            OmitXmlDeclaration = true,
        };

        public static void generate(string savePath)
        {
            XmlWriter writer = XmlWriter.Create(savePath, settings);
            writer.WriteStartElement("items");

            Parser.processedInstances.Sort
            (
               delegate (Parser.Item p1, Parser.Item p2)
               {
                   return p1.instance.CompareTo(p2.instance);
               }
           );

            for (int i = 0; i < Parser.processedInstances.Count; ++i)
            {
                writer.WriteStartElement("item");
                    writer.WriteStartElement("instance");
                        writer.WriteString(Parser.processedInstances[i].instance);
                    writer.WriteEndElement();
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();

            writer.Close();
        }
    }
}
