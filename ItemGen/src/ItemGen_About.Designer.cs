﻿namespace ItemGen
{
    partial class ItemGen_About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemGen_About));
            this.ItemGen_About_pictureBox = new System.Windows.Forms.PictureBox();
            this.ItemGen_About_label_Authors = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ItemGen_About_label_sourceCode = new System.Windows.Forms.Label();
            this.ItemGen_About_linkLabel_clickHere = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ItemGen_About_pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ItemGen_About_pictureBox
            // 
            this.ItemGen_About_pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("ItemGen_About_pictureBox.Image")));
            this.ItemGen_About_pictureBox.Location = new System.Drawing.Point(11, 34);
            this.ItemGen_About_pictureBox.Margin = new System.Windows.Forms.Padding(2);
            this.ItemGen_About_pictureBox.Name = "ItemGen_About_pictureBox";
            this.ItemGen_About_pictureBox.Size = new System.Drawing.Size(66, 62);
            this.ItemGen_About_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ItemGen_About_pictureBox.TabIndex = 0;
            this.ItemGen_About_pictureBox.TabStop = false;
            // 
            // ItemGen_About_label_Authors
            // 
            this.ItemGen_About_label_Authors.AutoSize = true;
            this.ItemGen_About_label_Authors.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ItemGen_About_label_Authors.Location = new System.Drawing.Point(101, 14);
            this.ItemGen_About_label_Authors.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ItemGen_About_label_Authors.Name = "ItemGen_About_label_Authors";
            this.ItemGen_About_label_Authors.Size = new System.Drawing.Size(79, 23);
            this.ItemGen_About_label_Authors.TabIndex = 1;
            this.ItemGen_About_label_Authors.Text = "Authors\r\n";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(101, 40);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 34);
            this.label1.TabIndex = 2;
            this.label1.Text = "-Patrix GG(44446385)\r\n-Alphonso\r\n";
            // 
            // ItemGen_About_label_sourceCode
            // 
            this.ItemGen_About_label_sourceCode.AutoSize = true;
            this.ItemGen_About_label_sourceCode.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ItemGen_About_label_sourceCode.Location = new System.Drawing.Point(101, 81);
            this.ItemGen_About_label_sourceCode.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ItemGen_About_label_sourceCode.Name = "ItemGen_About_label_sourceCode";
            this.ItemGen_About_label_sourceCode.Size = new System.Drawing.Size(122, 23);
            this.ItemGen_About_label_sourceCode.TabIndex = 3;
            this.ItemGen_About_label_sourceCode.Text = "Source code";
            // 
            // ItemGen_About_linkLabel_clickHere
            // 
            this.ItemGen_About_linkLabel_clickHere.AutoSize = true;
            this.ItemGen_About_linkLabel_clickHere.Font = new System.Drawing.Font("Arial", 10F);
            this.ItemGen_About_linkLabel_clickHere.Location = new System.Drawing.Point(106, 106);
            this.ItemGen_About_linkLabel_clickHere.Name = "ItemGen_About_linkLabel_clickHere";
            this.ItemGen_About_linkLabel_clickHere.Size = new System.Drawing.Size(71, 16);
            this.ItemGen_About_linkLabel_clickHere.TabIndex = 4;
            this.ItemGen_About_linkLabel_clickHere.TabStop = true;
            this.ItemGen_About_linkLabel_clickHere.Text = "Click here";
            this.ItemGen_About_linkLabel_clickHere.VisitedLinkColor = System.Drawing.Color.Blue;
            this.ItemGen_About_linkLabel_clickHere.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ItemGen_About_Link_LinkClicked);
            // 
            // ItemGen_About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 152);
            this.Controls.Add(this.ItemGen_About_linkLabel_clickHere);
            this.Controls.Add(this.ItemGen_About_label_sourceCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ItemGen_About_label_Authors);
            this.Controls.Add(this.ItemGen_About_pictureBox);
            this.Font = new System.Drawing.Font("Arial", 8F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ItemGen_About";
            this.ShowIcon = false;
            this.Text = "Info";
            this.Load += new System.EventHandler(this.ItemGen_About_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ItemGen_About_pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox ItemGen_About_pictureBox;
        private System.Windows.Forms.Label ItemGen_About_label_Authors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ItemGen_About_label_sourceCode;
        private System.Windows.Forms.LinkLabel ItemGen_About_linkLabel_clickHere;
    }
}