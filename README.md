# Introduction

**ItemGen** is a simple application made in C# to read deadalus scripts and generate list of items for G2O server saved in XML file.

## How to use it?

### Prerequisites

Before you run the app, make sure, that you have
decompiled daedalus scripts on your PC.
You can decompile the original game scripts or
mod scripts using a tool called **Gothic Sourcer**,
or download them from the internet.

Also, make sure, that you have archive tool,
for example **[7-zip](https://www.7-zip.org/)**.

### Installation

1. **[Download](https://gitlab.com/Patrix9999/ItemGen/raw/master/ItemGen/bin/Release/build.7z)** the archive file.
2. Unpack it wherever you want
3. Run the app

## Configurating application

To configure an application you have to edit config.xml file.

* Change checkForUpdates value to "true" for auto-updating application. To disable auto-updating change value to "false",
* Use lang option to change language of application. As a value you have to put "prefix"-"languageName". Application have builded four languages: English, Polish, German and Russian. To make a new language just create a new XML file in lang folder and name it using a pattern (prefix-langName.xml),

**Example file name**
```
en-English.xml
```
* savePath allow you to set a default path for saving XML files. After launching application this path will be loaded,
* includePaths allows you to set a default paths for including files. After launching application this paths will be loaded into list

**Example**
```xml
<includePaths>
    <path>C:/test</path>
</includePaths>
```
* excludePaths allows you to set a default paths for excluding files. After launching application this paths will be loaded into list

**Example**
```xml
<excludePaths>
    <path>C:/test</path>
</excludePaths>
```

## Build with

* [Microsoft Visual Studio](https://www.visualstudio.com/pl/) - IDE
* [.NET v.4.0.0](https://www.microsoft.com/net/) - Framework developed by Microsoft

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
